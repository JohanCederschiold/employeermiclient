import java.util.List;
import java.util.Properties;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import com.virtualpairprogrammers.employeemanagement.EmployeeManagementService;
import com.virtualpairprogrammers.employeemanagement.domain.Employee;

public class Main {
	
	
	
	public static void main (String [] args ) throws NamingException {
		
		
//		Generell klass som skickar nyckel/v�rdeparstyp.H�r s�tter vi properties f�r context nedan. 
		Properties jndiproperties = new Properties();
		
//		jndiproperties.put(Context.INITIAL_CONTEXT_FACTORY, "org.jboss.naming.remote.client.InitialContextFactory");
		jndiproperties.put(Context.INITIAL_CONTEXT_FACTORY, "org.wildfly.naming.client.WildFlyInitialContextFactory");
		jndiproperties.put(Context.PROVIDER_URL, "http-remoting://localhost:8080");
		jndiproperties.put("jboss.naming.client.ejb.context", true);
		
		Context jndi = new InitialContext(jndiproperties);
		
//		URL h�mtas fr�n Wildflys webgr�nssnitt. 
		EmployeeManagementService service = (EmployeeManagementService)jndi.lookup("EmployeeManagementServerApplication/EmployeeManagementImplementation!com.virtualpairprogrammers.employeemanagement.EmployeeManagementService");
		
		
//		Employee emplo = new Employee("Santa", "Claus", "Cowboy", 2000 );
//		
//		service.registerEmployee(emplo);
		
		
		System.out.println(service.getEmployeeById(2));
		
//		List<Employee> employees = service.getAllEmployees();
//		
//		for (Employee empl : employees ) {
//			System.out.println(empl);
//		}
		

//		List<Employee> searched = service.searchBySurname("Bob");
//		for (Employee sear : searched) {
//			System.out.println(sear);
//		}
		
		
	}  
	
	
	

}
